import os
from PIL import Image

# Change these values
MATCH_CSV = "output.txt"
SCREENS1 = "/home/jhoskins/projs/lsh/cs-data/screenshots/1.1.10"
SCREENS2 = "/home/jhoskins/projs/lsh/cs-data/screenshots/1.1.13"

f = open(MATCH_CSV)

# Iterate over the csv of form oldversionscreen,newversionscreen
for i, line in enumerate(f):
    file1, file2 = line.split(",")
    
    # Open the images corresponding to the match
    image1 = Image.open(SCREENS1 + os.path.sep + f1.strip())
    image2 = Image.open(SCREENS2 + os.path.sep + f2.strip())

    width1, height = i1.size
    width2, height = i2.size

    # Create a new image in the working directory, contacentating both images horizontally.
    new = Image.new("RGB", (width1 + width2, height))
    new.paste(im=i1, box=(0,0))
    new.paste(im=i2, box=(width1,0))
    new.save("new" + str(i) + ".png")
    
f.close()
