/*******************************************************************************
 * Copyright (c) 2016, SEMERU
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *******************************************************************************/

package Setup;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import org.apache.commons.io.FileUtils;

import edu.semeru.android.gcat.changes.Change;
import edu.semeru.android.gcat.changes.MissingComponentChange;
import edu.semeru.android.gcat.matching.MatchingAnalysis;
import edu.semeru.android.gcat.tree_builder.ConstantSettings;
import edu.semeru.android.gcat.tree_builder.UiTreeNode;

/**
 *
 * @author georgewpurnell This is the parent class for testing All tests should
 * extend GuiGitTest To use different values than the default, call
 * defaultTestSetup and then alter any parameter necessary before calling the
 * run method
 */
public class GcatTest {

    /**
     *
     */
    public int[] uiBoard = new int[]{0, 0, 1440, 2560};

    /**
     *
     */
    public int[] dsBoard = new int[]{0, 0, 1440, 2560};

    /**
     *
     */
    public int[][] ignoredComps = new int[][]{{0, 0, 1440, 100}, {0, 2372, 1440, 188}};

    /**
     *
     */
    public String prevParentPackage = "";

    /**
     *
     */
    public String newParentPackage = "";

    /**
     *
     */
    public String pathBase = "src" + File.separator + "test" + File.separator + "res" + File.separator;

    /**
     *
     */
    public String prevImgPath = "";

    /**
     *
     */
    public String prevXML = "";

    /**
     *
     */
    public String newXML = "";

    /**
     *
     */
    public String newImgPath = "";

    /**
     *
     */
    public MatchingAnalysis checker;
    
    /**
     *
     */
    public GcatTest() {
        // Nothing to do
    }
    

    /**
     * Sets up the default way of testing.
     *
     * @param newImgPath
     * @param newXML
     */
    public void defaultTestSetup(String newImgPath, String newXML) {
        ConstantSettings settings = ConstantSettings.getInstance();
        settings.setUIBoard(uiBoard);    // Default
        settings.setDSBoard(dsBoard);
        settings.setIgnoredCompDesign(ignoredComps);
        settings.setViolationThreshold(15);
        settings.setImgDiffThreshold(20);
        this.newImgPath = newImgPath;
        this.newXML = newXML;
    }

    /**
     * Method to execute the matching. After Run is complete, violation types
     * can be accessed through the checker variable.
     *
     * @param testNumber
     * @throws java.io.IOException
     */
    public void runMatchingAnalysis(String testNumber) throws IOException {
        String prevUiImage = pathBase + prevParentPackage + File.separator + prevImgPath;
        String prevUiDumpFile = pathBase + prevParentPackage + File.separator + prevXML;
        String newUiImage = pathBase + newParentPackage + File.separator + newImgPath;
        String newUiDumpFile = pathBase + newParentPackage + File.separator + newXML;

        Path currentRelativePath = Paths.get("");
        String currentFolder = currentRelativePath.toAbsolutePath().toString();
        String outputFolder = currentFolder + File.separator + "Outputs" + File.separator + prevParentPackage + testNumber + File.separator;

        File directory = new File(String.valueOf(outputFolder));
        if (!directory.exists()) {
            directory.mkdirs();
        } 
        // Delete contents of existing directory for easier testing
        else {
            FileUtils.cleanDirectory(directory); 
        }
        checker = new MatchingAnalysis(prevUiDumpFile, newUiDumpFile, prevUiImage, newUiImage, outputFolder);
        checker.RunChecker();
        checker.printOutViolations();
//        System.out.println("-- GUI Verfication Complete");
    }
    
    /**
     *
     * @param violationList
     * @param uiNode
     * @return
     */
    public Change findViolationWithNode(List<Change> violationList, UiTreeNode uiNode) {
//        System.out.println("Searching List: " + violationList + "for node: " + uiNode);
        for (Change violation : violationList) {
            if (violation.getNewNode().equals(uiNode))
                return violation;
        }
        return null;
    }
    
    // We need two, because missing component violations are treated differently by the violation manager.

    /**
     *
     * @param violationList
     * @param uiNode
     * @return
     */
    public MissingComponentChange findMissingComponentViolationWithNode(List<MissingComponentChange> violationList, UiTreeNode uiNode) {
//        System.out.println("Searching List: " + violationList + "for node: " + uiNode);
        for (MissingComponentChange violation : violationList) {
            if (violation.getOldNode().equals(uiNode))
                return violation;
        }
        return null;
    }
    
//    public boolean hasViolationWithNode(List<Violation> violationList, UiTreeNode uiNode) {
//        System.out.println("Searching List: " + violationList + "for node: " + uiNode);
//        for (Violation violation : violationList) {
//            if (violation.getViolationNode().equals(uiNode))
//                return true;
//        }
//        return false;
//    }
}
