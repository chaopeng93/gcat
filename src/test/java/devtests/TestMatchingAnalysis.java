/*******************************************************************************
 * Copyright (c) 2016, SEMERU
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *******************************************************************************/

package devtests;

/**
 * Acceptance tests for Summarizing GUI Changes.
 */
import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import org.junit.Test;

import edu.semeru.android.gcat.changes.ChangeType;
import edu.semeru.android.gcat.matching.MatchingAnalysis;
import edu.semeru.android.gcat.tree_builder.ConstantSettings;

import java.io.IOException;
import org.junit.Assert;

/**
 *
 * @author Louisa
 */
public class TestMatchingAnalysis {

    /**
     * Given two instances of the same file, for each, generate same tree and
     * match corresponding nodes correctly. No violations should occur.
     *
     * @throws java.io.IOException
     */
    @Test
    public void TestSameTree() throws IOException {
        ConstantSettings settings = ConstantSettings.getInstance();
        int[] uiBoard = new int[]{0, 0, 1440, 2368};
        settings.setUIBoard(uiBoard);    // Default
        int[] dsBoard = new int[]{0, 0, 1440, 2368};
        settings.setDSBoard(dsBoard);
        int[][] ignoredComps = new int[][]{{0, 0, 1440, 100}, {0, 2372, 1440, 188}};
        settings.setIgnoredCompDesign(ignoredComps);
        settings.setViolationThreshold(15);
        settings.setImgDiffThreshold(20);

        String prevUiImage = "src" + File.separator + "test" + File.separator + "res" + File.separator + "com.dropbox.android" + File.separator + "com.dropbox.android-original.png";
        String prevUiDumpFile = "src" + File.separator + "test" + File.separator + "res" + File.separator + "com.dropbox.android" + File.separator + "com.dropbox.android-original.xml";
        String newUiImage = "src" + File.separator + "test" + File.separator + "res" + File.separator + "com.dropbox.android" + File.separator + "com.dropbox.android-original.png";
        String newUiDumpFile = "src" + File.separator + "test" + File.separator + "res" + File.separator + "com.dropbox.android" + File.separator + "com.dropbox.android-original.xml";

        Path currentRelativePath = Paths.get("");
        String currentFolder = currentRelativePath.toAbsolutePath().toString();
        String outputFolder = currentFolder + File.separator + "Outputs" + File.separator + "com.dropbox.android" + File.separator;

        File directory = new File(String.valueOf(outputFolder));
        if (!directory.exists()) {
            directory.mkdirs();
        }

        MatchingAnalysis checker = new MatchingAnalysis(prevUiDumpFile, newUiDumpFile, prevUiImage, newUiImage, outputFolder);
        checker.RunChecker();
        checker.printOutViolations();
        System.out.println("-- GUI Verfication Complete");
        Assert.assertFalse(checker.hasViolationType(ChangeType.MISSING_COMPONENT));
    }

    /**
     *
     * @throws IOException
     */
    @Test
    public void TestDifferentTree() throws IOException {
        ConstantSettings settings = ConstantSettings.getInstance();
        int[] uiBoard = new int[]{0, 0, 1440, 2368};
        settings.setUIBoard(uiBoard);    // Default
        int[] dsBoard = new int[]{0, 0, 1440, 2368};
        settings.setDSBoard(dsBoard);
        int[][] ignoredComps = new int[][]{{0, 0, 1440, 100}, {0, 2372, 1440, 188}};
        settings.setIgnoredCompDesign(ignoredComps);
        settings.setViolationThreshold(15);
        settings.setImgDiffThreshold(20);

        String prevUiImage = "src" + File.separator + "test" + File.separator + "res" + File.separator + "com.apalon.ringtones" + File.separator + "com.apalon.ringtones-original.png";
        String prevUiDumpFile = "src" + File.separator + "test" + File.separator + "res" + File.separator + "com.apalon.ringtones" + File.separator + "com.apalon.ringtones-original.xml";
        String newUiImage = "src" + File.separator + "test" + File.separator + "res" + File.separator + "com.apalon.ringtones" + File.separator + "com.apalon.ringtones-original2-4.png";
        String newUiDumpFile = "src" + File.separator + "test" + File.separator + "res" + File.separator + "com.apalon.ringtones" + File.separator + "com.apalon.ringtones-ui-dump-2-4.xml";

        Path currentRelativePath = Paths.get("");
        String currentFolder = currentRelativePath.toAbsolutePath().toString();
        String outputFolder = currentFolder + File.separator + "Outputs" + File.separator + "com.apalon.ringtones" + File.separator;

        File directory = new File(String.valueOf(outputFolder));
        if (!directory.exists()) {
            directory.mkdirs();
        }

        MatchingAnalysis checker = new MatchingAnalysis(prevUiDumpFile, newUiDumpFile, prevUiImage, newUiImage, outputFolder);
        checker.RunChecker();
        checker.printOutViolations();
        System.out.println("-- GUI Verfication Complete");
        Assert.assertTrue(checker.hasViolationType(ChangeType.MISSING_COMPONENT));
    }

}
