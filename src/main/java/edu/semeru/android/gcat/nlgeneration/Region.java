/*******************************************************************************
 * Copyright (c) 2016, SEMERU
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *******************************************************************************/

package edu.semeru.android.gcat.nlgeneration;

import java.awt.Rectangle;

/**
 * Representation of a section of the screen, and the amount of change made to it.
 * @author jhoskins
 */
public class Region extends Rectangle implements Comparable {
    
    private double area; // Total area affected by changes in this region.
    private int index; // unique number relating it to the section of the overall board
    
    /**
     * Constructor.
     * 
     * @param x x coordinate
     * @param y y coordinate
     * @param width width
     * @param height height
     * @param index unique number related to section of the overall board
     */
    public Region(int x, int y, int width, int height, int index) {
        super(x, y, width, height);
        this.area = 0.0;
        this.index = index;
    }
    
    /**
     * Constructor. Default fields all set to 0
     */
    public Region() {
        super(0, 0, 0, 0);
        this.area = 0.0;
        this.index = 0;
    }
    
    /**
     * Increase the amount of change.
     * @param area the affected area of a change made in the region.
     */
    public void addArea(double area) {
        this.area += area;
    }
    
    /**
     * Get the total affected area of the changes in the region.
     * @return area
     */
    public double getArea() {
        return this.area;
    }
    
    /**
     * Get the unique ID of the region.
     * @return unique ID of the region
     */
    public int getIndex() {
        return this.index;
    }
    
    /**
     * Set the unique ID of the region.
     * @param index unique ID of the region
     */
    public void setIndex(int index) {
        this.index = index;
    }
    
    /**
     * Get the union of this rectangle and another region (including areas), and set the unique ID
     * of the new region.
     * @param r The region to unify with this region.
     * @param index The unique ID to set to the new region.
     * @return unique ID fo new region
     */
    public Region getUnion(Region r, int index) {
        Rectangle newRect = this.union(r);
        Region newRegion = new Region((int) newRect.getX(), (int) newRect.getY(), (int) newRect.getWidth(), (int) newRect.getHeight(), index);
        newRegion.addArea(r.getArea() + this.getArea());
        newRegion.setIndex(index);
        return newRegion;
    }

    /**
     * Compare the areas of two regions. Needed for sorting.
     * @param o Region to compare to
     * @return -1, 0, 1 corresponding to other area is smaller, equal, or larger.
     */
    @Override
    public int compareTo(Object o) {
        Region otherRegion = (Region) o;
        
        if (otherRegion.area < this.area) {
            return -1;
        } else if (otherRegion.area > this.area) {
            return 1;
        } else {
            return 0;
        }
    }
}
