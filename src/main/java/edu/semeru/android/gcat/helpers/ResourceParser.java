/*******************************************************************************
 * Copyright (c) 2016, SEMERU
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *******************************************************************************/

package edu.semeru.android.gcat.helpers;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.*;

import edu.semeru.android.gcat.tree_builder.ElementDocfile;

/**
 * Used to parse xml dump files
 * @author USBOLI
 *
 */
public class ResourceParser {

    //Resource layout folder
    String ResFolder = "";

    private HashMap<String, ElementDocfile> mapToElement = new HashMap<String, ElementDocfile>();

    /**
     * Retrieve map to element
     * @return mapToElement
     */
    public HashMap<String, ElementDocfile> getMapToElement() {
        return mapToElement;
    }

    /**
     * Constructor
     * @param folder resource folder
     */
    public ResourceParser(String folder) {
        this.ResFolder = folder;
    }

    /**
     * Parse file
     */
    public void runParser() {
        File input = new File(this.ResFolder);
        File[] st = input.listFiles();
        for (int i = 0; i < st.length; i++) {
            if (st[i].isFile()) {//other condition like name ends in html
                parse(st[i]);
            }
        }
    }

    private void parse(File input) {
        Document doc;
        try {
            doc = Jsoup.parse(input, "UTF-8", input.getAbsolutePath());
            doc.nodeName();
            Elements elms = doc.select(":empty:not(head)");
            for (Element elm : elms) {
                if (elm.attr("android:id").equals("")) {
                    continue;
                }
                String elementID = elm.attr("android:id");
                if (mapToElement.containsKey(elementID)) {
                    //System.out.println("found collision");
                }
                //record the last. Need to change later.
                mapToElement.put(elementID, new ElementDocfile(elm, input.getAbsolutePath()));
                //System.out.println(elementID);
            }
            //SurfaceView
        } catch (IOException e) {

        }
    }

}
