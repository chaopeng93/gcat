/*******************************************************************************
 * Copyright (c) 2016, SEMERU
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *******************************************************************************/

package edu.semeru.android.gcat.helpers;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;

import edu.semeru.android.gcat.GCatMain;

/**
 * @author KevinMoran
 *
 */
public class DataHelper {

    private static final int SCREEN_X = 0;
    private static final int SCREEN_Y = 0;
    private static final int SCREEN_WIDTH = 1199;
    private static final int SCREEN_HIEGHT = 1919;

    public static class Version implements Comparable<Version> {

        private String version;

        public final String get() {
            return this.version;
        }

        public Version(String version) {
            if(version == null)
                throw new IllegalArgumentException("Version can not be null");
            if(!version.matches("[0-9]+(\\.[0-9]+)*"))
                throw new IllegalArgumentException("Invalid version format");
            this.version = version;
        }

        @Override public int compareTo(Version that) {
            if(that == null)
                return 1;
            String[] thisParts = this.get().split("\\.");
            String[] thatParts = that.get().split("\\.");
            int length = Math.max(thisParts.length, thatParts.length);
            for(int i = 0; i < length; i++) {
                int thisPart = i < thisParts.length ?
                        Integer.parseInt(thisParts[i]) : 0;
                        int thatPart = i < thatParts.length ?
                                Integer.parseInt(thatParts[i]) : 0;
                                if(thisPart < thatPart)
                                    return -1;
                                if(thisPart > thatPart)
                                    return 1;
            }
            return 0;
        }

        @Override public boolean equals(Object that) {
            if(this == that)
                return true;
            if(that == null)
                return false;
            if(this.getClass() != that.getClass())
                return false;
            return this.compareTo((Version) that) == 0;
        }

    }

    static class ScreenPair{

        File oldScreenshot;
        File newScreeshot;
        File oldXmlFile;
        File newXmlFile;
        String packageName;


        /**
         * @param oldScreenshot
         * @param newScreeshot
         * @param oldXmlFile
         * @param newXmlFile
         */
        public ScreenPair(File oldScreenshot, File newScreeshot, File oldXmlFile, File newXmlFile, String packageName) {
            super();
            this.packageName = packageName;
            this.oldScreenshot = oldScreenshot;
            this.newScreeshot = newScreeshot;
            this.oldXmlFile = oldXmlFile;
            this.newXmlFile = newXmlFile;
        }

        /**
         * @return the packageName
         */
        public String getPackageName() {
            return packageName;
        }

        /**
         * @param packageName the packageName to set
         */
        public void setPackageName(String packageName) {
            this.packageName = packageName;
        }

        /**
         * @return the oldScreenshot
         */
        public File getOldScreenshot() {
            return oldScreenshot;
        }

        /**
         * @param oldScreenshot the oldScreenshot to set
         */
        public void setOldScreenshot(File oldScreenshot) {
            this.oldScreenshot = oldScreenshot;
        }

        /**
         * @return the newScreeshot
         */
        public File getNewScreeshot() {
            return newScreeshot;
        }

        /**
         * @param newScreeshot the newScreeshot to set
         */
        public void setNewScreeshot(File newScreeshot) {
            this.newScreeshot = newScreeshot;
        }

        /**
         * @return the oldXmlFile
         */
        public File getOldXmlFile() {
            return oldXmlFile;
        }

        /**
         * @param oldXmlFile the oldXmlFile to set
         */
        public void setOldXmlFile(File oldXmlFile) {
            this.oldXmlFile = oldXmlFile;
        }

        /**
         * @return the newXmlFile
         */
        public File getNewXmlFile() {
            return newXmlFile;
        }

        /**
         * @param newXmlFile the newXmlFile to set
         */
        public void setNewXmlFile(File newXmlFile) {
            this.newXmlFile = newXmlFile;
        }

    }

    public static void main (String[] args) {

        String pathToAppData = "/Users/KevinMoran/Dropbox/Documents/My_Graduate_School_Work/SEMERU/git_src_code/gitlab-code/Android-Summarizing-GUI-Changes/Artifacts/Apps_For-Evaluation";
        String pathToPIDData = "/Users/KevinMoran/Dropbox/Documents/My_Graduate_School_Work/SEMERU/git_src_code/gitlab-code/Android-Summarizing-GUI-Changes/Artifacts/Evaluation-Apps-PID-Images";
        String outputPath = "/Users/kevinmoran/Desktop/guigit-test";
        
//        String pathToAppData = "/home/jhoskins/projs/Android-Summarizing-GUI-Changes/Artifacts/Apps_For-Evaluation";
//        String pathToPIDData = "/home/jhoskins/projs/Android-Summarizing-GUI-Changes/Artifacts/Evaluation-Apps-PID-Images";
//        String outputPath = "/home/jhoskins/testOutput";
        
        List<ScreenPair> screens = parseAppsForStudy(pathToAppData);
        //processScreenPairsPID(screens, outputPath);
        //calculateDifferenceThresholds(outputPath);
        runGuiGitonEvaluationApps(screens, outputPath);

    }

    public static List<ScreenPair> parseAppsForStudy(String pathToAppData){

        List<ScreenPair> screenPairs = new ArrayList<>();

        File[] appDirectories = new File(pathToAppData).listFiles(File::isDirectory);

        System.out.println("Collecting App Data...");

        for(File currAppDir : appDirectories) {

            System.out.println(currAppDir.getAbsolutePath());

            File[] appData = currAppDir.listFiles(new FilenameFilter() {
                public boolean accept(File dir, String name) {
                    return name.toLowerCase().endsWith(".xml");
                }
            });

            List<Version> versionNumbers = new ArrayList<Version>();
            String oldVersionNumber = "0";
            String newVersionNumber = "0";
            float numberPairs = 0;
            String tempString;
            String rootAppPath = currAppDir.getAbsolutePath();
            String packageName = rootAppPath.substring(rootAppPath.lastIndexOf("/")+1);


            for(File currAppData : appData) {

                System.out.println(currAppData.getAbsolutePath());
                long count = currAppData.getName().chars().filter(ch -> ch == '-').count();
                System.out.println("Count of '-': " + count);

                if(count < 2) {

                    tempString = currAppData.getAbsolutePath();
                    tempString = tempString.substring(tempString.indexOf("uiautomator")+12, tempString.indexOf(".xml"));

                    System.out.println("Parsed Version #:" + tempString);
                    Version tempVersion = new Version(tempString);
                    versionNumbers.add(tempVersion);

                    oldVersionNumber = Collections.min(versionNumbers).get();
                    newVersionNumber = Collections.max(versionNumbers).get();
                    System.out.println("Old Version #:" + oldVersionNumber);
                    System.out.println("New Version #:" + newVersionNumber);
                    numberPairs += 0.5;

                }else {

                    numberPairs += 0.5;

                }
            }

            ScreenPair tempPair;
            String oldScreenshot;
            String newScreenshot;
            String oldXmlFile;
            String newXmlFile;

            System.out.println("Number of Pairs: " + numberPairs);

            for(int i = 0; i < numberPairs; i++) {


                if(i==0) {
                    oldScreenshot = rootAppPath + File.separator + "screen" + "-" + oldVersionNumber + ".png";
                    newScreenshot = rootAppPath + File.separator + "screen" + "-" + newVersionNumber + ".png";
                    oldXmlFile = rootAppPath + File.separator  + "uiautomator" + "-" + oldVersionNumber + ".xml";
                    newXmlFile = rootAppPath + File.separator  + "uiautomator" + "-" + newVersionNumber + ".xml";

                    tempPair = new ScreenPair(new File(oldScreenshot), new File(newScreenshot), new File(oldXmlFile), new File(newXmlFile), packageName);

                }else {

                    int currScreenNum = i+1;

                    oldScreenshot = rootAppPath + File.separator + "screen" +  "-" + oldVersionNumber + "-" + currScreenNum + ".png";
                    newScreenshot = rootAppPath + File.separator + "screen" + "-" + newVersionNumber + "-" + currScreenNum + ".png";
                    oldXmlFile = rootAppPath + File.separator  + "uiautomator" + "-" + oldVersionNumber + "-" + currScreenNum + ".xml";
                    newXmlFile = rootAppPath + File.separator  + "uiautomator" + "-" + newVersionNumber + "-" + currScreenNum + ".xml";

                    tempPair = new ScreenPair(new File(oldScreenshot), new File(newScreenshot), new File(oldXmlFile), new File(newXmlFile), packageName);

                }

                screenPairs.add(tempPair);

            }



        }


        return screenPairs;

    }


    public static void processScreenPairsPID(List<ScreenPair> screenPairs, String outputFolder) {

        for(ScreenPair currScreenPair : screenPairs) {

            String versionOld;
            String versionNew;
            String tempString;
            String outputPath = "";

            tempString = currScreenPair.getNewXmlFile().getAbsolutePath();
            System.out.println("??" + tempString);
            versionNew = tempString.substring(tempString.indexOf("uiautomator")+12, tempString.indexOf(".xml"));
            System.out.println("!!" + versionNew);

            tempString = currScreenPair.getOldXmlFile().getAbsolutePath();
            versionOld = tempString.substring(tempString.indexOf("uiautomator")+12, tempString.indexOf(".xml"));

            outputPath = outputFolder + File.separator + currScreenPair.getPackageName() + "-" + versionNew + "-vs-" + versionOld + ".png";

            System.out.println("Output Path: " + outputPath);
            System.out.println("NewScreenPath: " + currScreenPair.getNewScreeshot().getAbsolutePath());
            System.out.println("OldScreenPath: " + currScreenPair.getOldScreenshot().getAbsolutePath());

            System.out.println("Processing App: " + currScreenPair.getPackageName());

            try {
                PIDHelper.generateDifferenceImage(currScreenPair.getNewScreeshot().getAbsolutePath(), currScreenPair.getOldScreenshot().getAbsolutePath(), outputPath);
            } catch (InterruptedException | IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }


        }

    }

    public static void calculateDifferenceThresholds(String pathtoPIDImages){ 

        File outputFolderFile = new File(pathtoPIDImages);

        File[] PIDImages = outputFolderFile.listFiles(new FilenameFilter() {
            public boolean accept(File dir, String name) {
                return name.toLowerCase().endsWith(".png");
            }
        });

        float pixelDifference = 0;

        DescriptiveStatistics pixelDifferenceStats = new DescriptiveStatistics();

        for (File currPIDImage : PIDImages) {

            try {
                pixelDifference = PIDHelper.getPixelDifference(currPIDImage.getAbsolutePath(), SCREEN_X, SCREEN_Y, SCREEN_HIEGHT, SCREEN_WIDTH);
                pixelDifferenceStats.addValue(pixelDifference);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            System.out.println(currPIDImage.getName() + " PixelDifference: " + pixelDifference);

        }

        double mean = pixelDifferenceStats.getMean();
        double median = pixelDifferenceStats.getPercentile(50);
        double stdDev = pixelDifferenceStats.getStandardDeviation();

        System.out.println();
        System.out.println("PID Data Statistics:");
        System.out.println("Mean Pixel Difference: " + mean);
        System.out.println("Median Pixel Difference: " + median);
        System.out.println("Pixel Difference Standard Deviation: " + stdDev);

    }


    public static void runGuiGitonEvaluationApps(List<ScreenPair> screenPairs, String outputFolder) {

        String versionOne;
        String versionTwo;
        String outputFolderPath = "";
        
        for(ScreenPair currScreenPair : screenPairs) {
                        
            versionOne = currScreenPair.getOldXmlFile().getAbsolutePath();
            versionOne = versionOne.substring(versionOne.indexOf("uiautomator")+12, versionOne.indexOf(".xml"));
            
            versionTwo = currScreenPair.getNewXmlFile().getAbsolutePath();
            versionTwo = versionTwo.substring(versionTwo.indexOf("uiautomator")+12, versionTwo.indexOf(".xml"));
            
            outputFolderPath = outputFolder + File.separator + currScreenPair.getPackageName() + "-" + versionOne + "-" + versionTwo;
            
            System.out.println("Package Name: " + currScreenPair.getPackageName() + "Version 1: " + versionOne + " Version 2: " + versionTwo);
            
            GCatMain.runGuiGit(currScreenPair.getOldScreenshot().getAbsolutePath(), currScreenPair.getOldXmlFile().getAbsolutePath(), currScreenPair.getNewScreeshot().getAbsolutePath(), currScreenPair.getNewXmlFile().getAbsolutePath(), "0,0,1200,48:0,1826,1200,95", outputFolderPath, true, 15, 20);
            try {
                FileUtils.deleteDirectory(new File("/Volumes/Macintosh_HD_3/Dropbox/Documents/My_Graduate_School_Work/SEMERU/git_src_code/gitlab-code/Android-Summarizing-GUI-Changes/Code/gcat/html/res/images/prev"));
                FileUtils.deleteDirectory(new File("/Volumes/Macintosh_HD_3/Dropbox/Documents/My_Graduate_School_Work/SEMERU/git_src_code/gitlab-code/Android-Summarizing-GUI-Changes/Code/gcat/html/res/images/new"));
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            
        }

    }

}
