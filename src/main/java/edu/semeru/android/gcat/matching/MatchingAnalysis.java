/*******************************************************************************
 * Copyright (c) 2016, SEMERU
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *******************************************************************************/

package edu.semeru.android.gcat.matching;

import com.rits.cloning.Cloner;

import edu.semeru.android.gcat.changes.*;
import edu.semeru.android.gcat.helpers.ImagesHelper;
import edu.semeru.android.gcat.helpers.PIDHelper;
import edu.semeru.android.gcat.helpers.ResourceParser;
import edu.semeru.android.gcat.tree_builder.BasicTreeNode;
import edu.semeru.android.gcat.tree_builder.ConstantSettings;
import edu.semeru.android.gcat.tree_builder.RootUINode;
import edu.semeru.android.gcat.tree_builder.UIDumpParser;
import edu.semeru.android.gcat.tree_builder.UiTreeNode;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.io.FileUtils;

/**
 * This is the master class of Android GUI Checker. The class manages and
 * controls all building blocks for the project.
 *
 * @author Boyang Li Created on Aug 07, 2016
 */
public class MatchingAnalysis extends Thread {

    /**
     * Xml file for old commit
     */
    public static String uiDumpFilePrev = "";

    /**
     * Xml file for new commit
     */
    public static String uiDumpFileNew = "";

    /**
     * Screenshot path of new commit
     */
    public static String newScreenShot = "";

    /**
     * Screenshot path of old commit
     */
    public static String prevScreenShot = "";

    /**
     * Output folder path
     */
    public static String outputFolder = "";

    /**
     * Resource folder for old commit
     */
    public static String prevResFolder = "";

    /**
     * Resource folder for new commit
     */
    public static String newResFolder = "";
    
    /**
     * File path for old commit image
     */
    public static String prevImageJpg = "html" + File.separator + "res" + 
            File.separator + "images" + File.separator + "prevImage.jpg";

    /**
     * File path for new commit image
     */
    public static String newImageJpg = "html" + File.separator + "res" + 
            File.separator + "images" + File.separator + "newImage.jpg";

    /**
     * Template for commit trees
     */
    public static String treeHtmlTemplate = "";

    /**
     * File path for PID output
     */
    public static String PIDImagePath = "html" + File.separator + "res" + 
            File.separator + "images" + File.separator + "PIDOutput.png";
    
    private RootUINode prevTreeRoot;
    private RootUINode newTreeRoot;
    private RootUINode commonTreeRoot;
    private ChangeManager vManager;
    
    private static String pidBwPath = "html" + File.separator + "res" + 
            File.separator + "images" + File.separator + "pidbw" + File.separator;
    private static String prevImagePath = "res" + 
            File.separator + "images" + File.separator + "prevImage.jpg";
    private static String newImagePath = "res" + 
            File.separator + "images" + File.separator + "newImage.jpg";

//	public static HashSet<InvalidOverlap> warnings = new HashSet<InvalidOverlap>();
    /**
     * Constructor and check path existence.
     *
     * @param uiDumpFilePrev Xml file for old commit
     * @param uiDumpFileNew Xml file for new commit
     * @param prevScreenshot screenshot path for old commit
     * @param newScreenshot screenshot path for new commit
     * @param outputFolder output folder path
     */
    public MatchingAnalysis(String uiDumpFilePrev, String uiDumpFileNew, String prevScreenshot, String newScreenshot, String outputFolder) {
        MatchingAnalysis.uiDumpFilePrev = uiDumpFilePrev;
        MatchingAnalysis.uiDumpFileNew = uiDumpFileNew;
        MatchingAnalysis.newScreenShot = newScreenshot;
        MatchingAnalysis.prevScreenShot = prevScreenshot;
        MatchingAnalysis.outputFolder = outputFolder;
        this.vManager = new ChangeManager(newScreenshot, outputFolder);
        //MatchingAnalysis.warnings.clear();

        File directory = new File(String.valueOf(outputFolder));
        if (!directory.exists()) {
            directory.mkdir();
        }

        ImagesHelper.TransferPNGToJPG(prevScreenShot, prevImageJpg);
        ImagesHelper.TransferPNGToJPG(newScreenShot, newImageJpg);
    }

    @Override
    public void run() {
        RunChecker();
        try {
            printOutViolations();
        } catch (IOException ex) {
            Logger.getLogger(MatchingAnalysis.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Build the trees describing the GUI XML hierarchies, run our change analysis
     * algorithm, and generate a report summarizing the changes.
     */
    public void RunChecker() {
        //System.out.println("-- Processing Design and App GUI-Structures");

        // 1. Parse previous ui dump file
        UIDumpParser parserPrev = new UIDumpParser();
        prevTreeRoot = parserPrev.parseXml(uiDumpFilePrev); //change the file at some point
        prevTreeRoot.transform();

        // 2. Parse new ui dump file
        UIDumpParser parser = new UIDumpParser();
        newTreeRoot = parser.parseXml(uiDumpFileNew);
        newTreeRoot.transform();

        // 3. Parse resource - source codes for previous commit
        ResourceParser prevResParser;
        if (!MatchingAnalysis.prevResFolder.equals("")) {
            prevResParser = new ResourceParser(MatchingAnalysis.prevResFolder);
            prevResParser.runParser();
            prevTreeRoot.assignRes(prevResParser.getMapToElement());
        }

        // 4. Parse resource - source codes for new commit
        ResourceParser newResParser;
        if (!MatchingAnalysis.newResFolder.isEmpty()) {
            newResParser = new ResourceParser(MatchingAnalysis.newResFolder);
            newResParser.runParser();
            newTreeRoot.assignRes(newResParser.getMapToElement());
        }

        //Print out trees
//        System.out.println("-------------- Prev Tree --------------");
//        prevTreeRoot.printOutTree();
//        System.out.println("-------------- New Tree --------------");
//        newTreeRoot.printOutTree();
        ConstantSettings settings = ConstantSettings.getInstance();
        //System.out.println("!!!Matching Difference: " + settings.getThresholdMatchDistance());

        MatchingNodes tMatching = new MatchingNodes(prevTreeRoot, newTreeRoot);
        tMatching.matching();

        prevTreeRoot.cropScreen("prev");
        newTreeRoot.cropScreen("new");

        PIDAnalysis();
        
        System.out.println("Beginning analysis...");
        //Check designRoot leaf nodes
        CheckAllViolations();
        prevTreeRoot.printOutHTMLTree("prev");
        newTreeRoot.printOutHTMLTree("new");
        /*code to get and print common tree*/
        List<Change> violations = vManager.getAllViolations();
        List<UiTreeNode> violationNodes = new ArrayList<UiTreeNode>();
        for (Change v : violations) {        //get a list of all nodes with violations
            violationNodes.add(v.getNewNode());
        }

        List<MissingComponentChange> missing = vManager.getMissingComponents();
        List<UiTreeNode> missingNodes = new ArrayList<UiTreeNode>();
        for (Change v : missing) {        //get a list of all nodes with violations
            missingNodes.add(v.getNewNode());
        }
        
        // Create common tree
        Cloner cloner = new Cloner();
        commonTreeRoot = (RootUINode) cloner.deepClone(newTreeRoot);//makes a deep copy of new TreeRoot
        int i = 0;
        for (UiTreeNode uiNode : commonTreeRoot.getLeafNodes()) { //iterate through common tree leaf nodes remove nodes from tree if they contain a violation
            if (uiNode != null) {
                if (violationNodes.contains(newTreeRoot.getLeafNodes().get(i)) || missingNodes.contains(newTreeRoot.getLeafNodes().get(i))) {
                    BasicTreeNode mParent = uiNode.getmParent();
                    ArrayList<BasicTreeNode> mChildren = mParent.getmChildren();
                    mChildren.remove(uiNode);
                }
            }
            i++;
        }

        System.out.println("Complete!");
        System.out.println("Generating Report...");
        System.out.println();
        System.out.println("-------------- Common Tree --------------");
        commonTreeRoot.printOutTree();
        commonTreeRoot.printOutHTMLTree("common");  
    }

    /**
     * Print out all types of violations and save into output folder
     * @throws java.io.IOException
     */
    public void printOutViolations() throws IOException {
        this.vManager.printOutViolations(prevImagePath, newImagePath, "");
        
        // Move HTML folder into outputs
        File htmlSource = new File("html");
        File htmlDest = new File(outputFolder + File.separator + "html");
        FileUtils.copyDirectory(htmlSource, htmlDest);

    }

    /**
     * Run a perceptual differencing algorithm on the two GUI screenshots, and 
     * generate output that describes differences between the two images.
     */
    public void PIDAnalysis() {
        //1. Generate PID image
        try {
            PIDHelper.generateDifferenceImage(newScreenShot, prevScreenShot, PIDImagePath);
        } catch (InterruptedException | IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        //2. Process the file and add the info to prevTreeRoot
        ArrayList<UiTreeNode> leafNodesPrev = this.prevTreeRoot.getLeafNodes();
        for (UiTreeNode node : leafNodesPrev) {
            float diff;
            try {
                diff = PIDHelper.getPixelDifference(PIDImagePath, node.getX(), node.getY(), node.getHeight(), node.getWidth());
                //if (diff != 0){
                    //System.out.println(node.printOutGUICheckerFormat() + " " + diff + "%");
                //}
                node.setPIDDiffRatio(diff);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                //e.printStackTrace();
            } catch (ArrayIndexOutOfBoundsException ex) {
                //ex.printStackTrace();
                //System.out.println("");
            }
        }

        // 3. Process the file and add the info to newTreeRoot
        ArrayList<UiTreeNode> leafNodesUi = this.newTreeRoot.getLeafNodes();
        for (UiTreeNode node : leafNodesUi) {
            float diff;
            try {
                diff = PIDHelper.getPixelDifference(PIDImagePath, node.getX(), node.getY(), node.getHeight(), node.getWidth());
                //if (diff != 0){
                    //System.out.println(node.printOutGUICheckerFormat() + " " + diff + "%");
                //}
                node.setPIDDiffRatio(diff);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                //e.printStackTrace();
            } catch (ArrayIndexOutOfBoundsException ex) {
                //ex.printStackTrace();
                //System.out.println("");
            }
        }

    }

    /**
     * Get a List of all violations of a specific type. To get all missing
     * component violations, you must use getMissingComponents() instead.
     *
     * @param type The type of violation you want all occurrences of.
     * @return List of Violations
     */
    public List<Change> getViolationsOfType(ChangeType type) {
        return vManager.getViolationsOfType(type);
    }

    /**
     * Get all missing component violations. 
     * @return list of missing components
     */
    public List<MissingComponentChange> getMissingComponents() {
        return vManager.getMissingComponents();
    }
    
    /**
     * Checks if a violation of type vt has been detected.
     * @param vt violation type
     * @return true if violation type has been detected
     */
    public boolean hasViolationType(ChangeType vt) {
        return vManager.hasViolationType(vt);
    }

    /**
     * Runs our comparison algorithm on the the hierarchical xml trees and the 
     * generated PID output. Builds a list of all changes between the two GUIs.
     */
    private void CheckAllViolations() {
        ArrayList<UiTreeNode> leafNodesNew = newTreeRoot.getLeafNodes();
        ArrayList<UiTreeNode> leafNodesPrev = prevTreeRoot.getLeafNodes();

        for (int i = 0; i < leafNodesNew.size(); i++) {
            if (!(leafNodesNew.get(i).getType() == null) && leafNodesNew.get(i).getType().contains("Layout")) {
                //System.out.println(leafNodesNew.get(i).getType());
                leafNodesNew.remove(i);
            }
        }

        for (int i = 0; i < leafNodesPrev.size(); i++) {
            if (!(leafNodesPrev.get(i).getType() == null) && leafNodesPrev.get(i).getType().contains("Layout")) {
//				System.out.println(leafNodesPrev.get(i).getType());
                leafNodesPrev.remove(i);
            }
        }

        ConstantSettings settings = ConstantSettings.getInstance();
        //extra components implemented
        for (UiTreeNode uiNode : leafNodesNew) {
            if (uiNode.getCorrespondingNode() == null) {
                if (uiNode.getPIDDiffRatio() != 0) {
                    uiNode.setHasViolation();
                    vManager.addViolations(new ExtraComponentChange(uiNode));
                }
                //System.out.println("extra node" + uiNode);
            }
        }

        for (UiTreeNode dsNode : leafNodesPrev) {
            UiTreeNode uiNode = (UiTreeNode) dsNode.getCorrespondingNode();  //uiNode shouldn't be null

            //Check missing component
            if (uiNode == null) {
                //if PID has different
                if (dsNode.getPIDDiffRatio() != 0) {
                    dsNode.setHasViolation();
                    vManager.addViolations(new MissingComponentChange(dsNode));
                }
                continue;
            } else {
                ///System.out.println(dsNode + "  --->  ui node : " + uiNode);
            }

            String dsNodeCroppedImg = dsNode.getCroppedImagePath();
            String uiNodeCroppedImg = uiNode.getCroppedImagePath();

            //Check image/background diff. SHOULD CHECK first 
            /*
            System.out.println("New Release Tree Text: " + uiNode.getName().trim().replaceAll("\n", "").replaceAll(" ", "")); //For debugging
            System.out.println("Previous Tree Text: " + dsNode.getName().trim().replaceAll("\n", "").replaceAll(" ", ""));
            System.out.println("PID Difference: " + dsNode.getPIDDiffRatio());
            System.out.println("Threshold: " + settings.getImgDiffThreshold());
             */
            if (dsNode.getPIDDiffRatio() > settings.getImgDiffThreshold() && !uiNode.getType().equals("TextView")) {
                //System.out.println(uiNode.getName() + " uiNodeType: " + uiNode.getType());
                boolean areHistogramsClose = false;
                boolean areImagesSame = false;
                float diff = 0;
                if (!dsNodeCroppedImg.equals("") && !uiNodeCroppedImg.equals("")) {
                    try {
                        areHistogramsClose = ImagesHelper.areHistogramsClose(dsNodeCroppedImg, uiNodeCroppedImg);
                        String pidOutput = pidBwPath + Math.random() + ".png";
                        PIDHelper.generateDifferenceImageBW(uiNodeCroppedImg, dsNodeCroppedImg, outputFolder, pidOutput);
                        File pidOutputFile = new File(pidOutput);
                        if (pidOutputFile.exists()) {
                            diff = PIDHelper.getPixelDifference(pidOutput, 0, 0, uiNode.getHeight(), uiNode.getWidth());
                        }
                        //System.out.println("BW Image Diff: " + diff);
                        if (diff <= 10) {
                            areImagesSame = true;
                        }
                    } catch (ArithmeticException ex) {
                        //System.out.println("ArithmeticException  ignored"); //TODO:
                    } catch (InterruptedException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    } catch (IOException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
                uiNode.setHasViolation();
                dsNode.setHasViolation();
                vManager.addViolations(new ImageChange(dsNode, uiNode, dsNode.getPIDDiffRatio(), areHistogramsClose, areImagesSame));
            }

            //Check string mismatch violation
            if (uiNode.getType().equals("TextView") && !uiNode.getName().trim().replaceAll("\n", "").replaceAll(" ", "").equals(dsNode.getName().trim().replaceAll("\n", "").replaceAll(" ", ""))) {
                uiNode.setHasViolation();
                dsNode.setHasViolation();
                vManager.addViolations(new TextChange(uiNode, dsNode.getName()));
            }

            //Check size diff
            //int diff = (int) dsNode.getCentric().computeDistance(uiNode.getCentric());
            //if (diff > 0) {
            //        vManager.addViolations(new ViolationLayoutRelation(uiNode, Edge.Absolute, dsNode.getCentric(), uiNode.getCentric()));
            //}
            //Check left and top
            int diffx = Math.abs(dsNode.getX() - uiNode.getX()); //new - prev
            int diffy = Math.abs(dsNode.getY() - uiNode.getY());
            if (diffx > 0 || diffy > 0) {
                uiNode.setHasViolation();
                dsNode.setHasViolation();
                vManager.addViolations(new LocationChange(uiNode, dsNode));
            }

            //Check size width
            int diff = Math.abs(dsNode.getWidth() - uiNode.getWidth()); //ideal - real;
            if (diff > 0) {
                uiNode.setHasViolation();
                dsNode.setHasViolation();
                vManager.addViolations(new ComponentSizeChange(uiNode, false, dsNode.getWidth(), uiNode.getWidth()));
            }
            //	}

            //Check size height
            diff = Math.abs(dsNode.getHeight() - uiNode.getHeight()); //ideal - real;
            if (diff > 0) {
                uiNode.setHasViolation();
                dsNode.setHasViolation();
                vManager.addViolations(new ComponentSizeChange(uiNode, true, dsNode.getHeight(), uiNode.getHeight()));
            }
            //}

            //Font violation                       
            if (vManager.getNumberOfValidViolation(uiNode) == 0 && uiNode.getType().equals("TextView") && dsNode.getPIDDiffRatio() > settings.getViolationThreshold()/4){
                //System.out.println("\n dsNode: " + dsNode.getPIDDiffRatio() + " uiNode: " + uiNode.getPIDDiffRatio());
                vManager.addViolations(new FontChange(dsNode, uiNode, dsNode.getPIDDiffRatio()));
            }

            //Check text color violation only if 1)PID > the threshold *3 2)Text view
            if (dsNode.getPIDDiffRatio() > settings.getViolationThreshold() && dsNode.getType().equals("TextView") && uiNode.getType().equals("TextView")) {
                boolean areHistogramsClose = true;
                //System.out.println("\n \n Threshold for color change: " + settings.getViolationThreshold() + "Diff ratio: " + dsNode.getPIDDiffRatio() + " \n \n ");
                if (!dsNodeCroppedImg.isEmpty() && !uiNodeCroppedImg.isEmpty()) {
                    try {
                        areHistogramsClose = ImagesHelper.areHistogramsClose(dsNodeCroppedImg, uiNodeCroppedImg);
                    } catch (ArithmeticException ex) {
                        //System.out.println("ArithmeticException  ignored"); //TODO:
                    }
                }
//                System.out.println("AreHistogramsClose: " + areHistogramsClose);
                if (!areHistogramsClose) {
                    uiNode.setHasViolation();
                    dsNode.setHasViolation();
                    vManager.addViolations(new TextColorChange(uiNode));
                }
            }
            if (!dsNode.getType().equals(uiNode.getType())){
                uiNode.setHasViolation();
                dsNode.setHasViolation();
                //vManager.addViolations(new ViolationNodeType(uiNode));
            }

            //TODO: add other violations*/
        }
    }
}
