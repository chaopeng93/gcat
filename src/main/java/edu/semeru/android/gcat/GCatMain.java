/*******************************************************************************
 * Copyright (c) 2016, SEMERU
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *******************************************************************************/

package edu.semeru.android.gcat;

import java.awt.Desktop;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import javax.imageio.ImageIO;

import edu.semeru.android.gcat.matching.MatchingAnalysis;
import edu.semeru.android.gcat.tree_builder.ConstantSettings;
import net.sourceforge.argparse4j.ArgumentParsers;
import net.sourceforge.argparse4j.impl.Arguments;
import net.sourceforge.argparse4j.inf.ArgumentParser;
import net.sourceforge.argparse4j.inf.ArgumentParserException;
import net.sourceforge.argparse4j.inf.Namespace;

/**
 *
 * @author jhoskins
 */
public class GCatMain {

    /**
     * Run GUI change differencing tool
     * @param args the command line arguments
     * @throws java.io.IOException
     */
    public static void main(String[] args) throws IOException {

        ArgumentParser parser = ArgumentParsers.newFor("GCAT").build()
                .defaultHelp(true)
                .description("Summarizes the differences between Android GUIs in natural language");

        // User arguments
        parser.addArgument("prevUiImage");
        parser.addArgument("prevUiDumpFile");
        parser.addArgument("newUiImage");
        parser.addArgument("newUiDumpFile");
        // Optional arguments
        parser.addArgument("--htmlreport").action(Arguments.storeTrue());
        parser.addArgument("--violationthreshold").type(Integer.class).setDefault(15);
        parser.addArgument("--imgDiffthreshold").type(Integer.class).setDefault(20);
        parser.addArgument("--ignoredareas").type(String.class).setDefault("0,0,1440,100:0,2372,1440,188");
        Namespace ns = null;
        
        try {
            ns = parser.parseArgs(args);
        } catch (ArgumentParserException e) {
            parser.handleError(e);
        }

        // Make sure the file exists
        File prevUiImageFile = new File(ns.getString("prevUiImage"));
        if (!prevUiImageFile.exists()) {
            System.out.println("File " + ns.getString("prevUiImage") + " does not exist.");
            System.exit(0);
        }

        File prevUiDumpFile = new File(ns.getString("prevUiDumpFile"));
        if (!prevUiDumpFile.exists()) {
            System.out.println("File " + ns.getString("prevUiDumpFile") + " does not exist.");
            System.exit(0);
        }

        File newUiImageFile = new File(ns.getString("newUiImage"));
        if (!newUiImageFile.exists()) {
            System.out.println("File " + ns.getString("newUiImage") + " does not exist.");
            System.exit(0);
        }

        File newUiDumpFile = new File(ns.getString("newUiDumpFile"));
        if (!newUiDumpFile.exists()) {
            System.out.println("File " + ns.getString("newUiDumpFile") + " does not exist.");
            System.exit(0);
        }

        ConstantSettings settings = ConstantSettings.getInstance();

        try {
            BufferedImage uiscreen = ImageIO.read(newUiImageFile);
            BufferedImage dsscreen = ImageIO.read(prevUiImageFile);
            int[] uiBoard = new int[]{0, 0, uiscreen.getWidth(), uiscreen.getHeight()};
            int[] dsBoard = new int[]{0, 0, dsscreen.getWidth(), dsscreen.getHeight()};
            settings.setUIBoard(uiBoard);
            settings.setDSBoard(dsBoard);
        } catch (Exception e) {
            e.printStackTrace();
        }

        System.out.println();
        int[][] ignoredComps = getignoredDimensions(ns.getString("ignoredareas")); //default
        settings.setIgnoredCompDesign(ignoredComps);
        settings.setViolationThreshold(ns.getInt("violationthreshold")); //default
        settings.setImgDiffThreshold(ns.getInt("imgDiffthreshold")); //default
        System.out.println("Violation threshold set to " + settings.getViolationThreshold() + ".");
        System.out.println("Imgage difference threshold set to " + settings.getImgDiffThreshold() + ".");
        System.out.println("Ignored screen areas set to " + Arrays.deepToString(settings.getIgnoredCompDesign()) + ".");

        System.out.println();
        System.out.println("Initializing...");
        
        Path currentRelativePath = Paths.get("");
        String currentFolder = currentRelativePath.toAbsolutePath().toString();
        String outputFolder = currentFolder + File.separator + "Outputs" + File.separator;

        MatchingAnalysis checker = new MatchingAnalysis(ns.getString("prevUiDumpFile"), ns.getString("newUiDumpFile"), ns.getString("prevUiImage"), ns.getString("newUiImage"), outputFolder);

        checker.RunChecker();
        checker.printOutViolations();

        // Open html once GCAT finishes
        if (ns.getBoolean("htmlreport")) {
            if (Desktop.isDesktopSupported()) {
                System.out.println("Opening change report in browser...");
                try {
                    String filePath = "file://" + new File(".").getCanonicalPath() + File.separator+ "html" + File.separator +"Full-Report.html";
                    Desktop.getDesktop().browse(new URI(filePath));
                } catch (Exception ex) {
                    ex.printStackTrace();
                    System.out.println("Could not open report. See the html folder"
                            + "to view your changes");
                    System.exit(0);
                }
            } else {
                System.out.println("Could not open report. See the html folder"
                        + "to view your changes");
            }
        }

    }

    public static void runGuiGit(String prevUiImage, String prevUiDump, String newUiImage, String newUiDump, String ignoredAreas, String outputFolder, boolean htmlReport, int violationthreshold, int imgDiffThreshold) {
        
        // Make sure the file exists
        File prevUiImageFile = new File(prevUiImage);
        if (!prevUiImageFile.exists()) {
            System.out.println("File " + prevUiImage + " does not exist.");
            System.exit(0);
        }

        File prevUiDumpFile = new File(prevUiDump);
        if (!prevUiDumpFile.exists()) {
            System.out.println("File " + prevUiDump + " does not exist.");
            System.exit(0);
        }

        File newUiImageFile = new File(newUiImage);
        if (!newUiImageFile.exists()) {
            System.out.println("File " + newUiImage + " does not exist.");
            System.exit(0);
        }

        File newUiDumpFile = new File(newUiDump);
        if (!newUiDumpFile.exists()) {
            System.out.println("File " + newUiDump + " does not exist.");
            System.exit(0);
        }

        ConstantSettings settings = ConstantSettings.getInstance();

        try {
            BufferedImage uiscreen = ImageIO.read(newUiImageFile);
            BufferedImage dsscreen = ImageIO.read(prevUiImageFile);
            int[] uiBoard = new int[]{0, 0, uiscreen.getWidth(), uiscreen.getHeight()};
            int[] dsBoard = new int[]{0, 0, dsscreen.getWidth(), dsscreen.getHeight()};
            settings.setUIBoard(uiBoard);
            settings.setDSBoard(dsBoard);
        } catch (Exception e) {
            e.printStackTrace();
        }

        System.out.println();
        int[][] ignoredComps = getignoredDimensions(ignoredAreas); //default
        settings.setIgnoredCompDesign(ignoredComps);
        settings.setViolationThreshold(violationthreshold); //default
        settings.setImgDiffThreshold(imgDiffThreshold); //default
        System.out.println("Violation threshold set to " + settings.getViolationThreshold() + ".");
        System.out.println("Imgage difference threshold set to " + settings.getImgDiffThreshold() + ".");
        System.out.println("Ignored screen areas set to " + Arrays.deepToString(settings.getIgnoredCompDesign()) + ".");

        System.out.println();
        System.out.println("Initializing...");
        
        Path currentRelativePath = Paths.get("");
        String currentFolder = currentRelativePath.toAbsolutePath().toString();

        MatchingAnalysis checker = new MatchingAnalysis(prevUiDump, newUiDump, prevUiImage, newUiImage, outputFolder);

        checker.RunChecker();
        try {
            checker.printOutViolations();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        // Open html once GCAT finishes
//        if (htmlReport) {
//            if (Desktop.isDesktopSupported()) {
//                System.out.println("Opening change report in browser...");
//                try {
//                    String filePath = "file://" + new File(".").getCanonicalPath() + File.separator+ "html" + File.separator +"Full-Report.html";
//                    Desktop.getDesktop().browse(new URI(filePath));
//                } catch (Exception ex) {
//                    ex.printStackTrace();
//                    System.out.println("Could not open report. See the html folder"
//                            + "to view your changes");
//                    System.exit(0);
//                }
//            } else {
//                System.out.println("Could not open report. See the html folder"
//                        + "to view your changes");
//            }
//        }
        
    }
    
    /**
     * Converts a string formatted as x,y,width,height:xn,yn,widthn,heightn:... into 
     * an array holding subarrays of each region. Regions are defined as the 
     * rectangle starting at x, y with the given width and height.
     * @param dimens A string describing all regions to be ignored.
     * @return all regions to be ignored formatted as an array of length-4 arrays.
     */
    public static int[][] getignoredDimensions(String dimens) {

        String[] firstArray = dimens.split(":");

        int[][] screenDimensFinal = new int[firstArray.length][4];

        for (int i = 0; i < firstArray.length; i++) {

            //System.out.println(firstArray[i]);
            String secondArray[] = firstArray[i].split(",");

            for (int j = 0; j < secondArray.length; j++) {

                screenDimensFinal[i][j] = Integer.parseInt(secondArray[j]);
                //System.out.println(secondArray[j]);

            }

        }
        return screenDimensFinal;
    }
}
