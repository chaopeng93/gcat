/*******************************************************************************
 * Copyright (c) 2016, SEMERU
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *******************************************************************************/

package edu.semeru.android.gcat.screen_matching;

import ij.process.ColorProcessor;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import static java.awt.image.BufferedImage.TYPE_INT_ARGB;
import static java.awt.image.BufferedImage.TYPE_INT_RGB;
import java.awt.image.DataBufferByte;
import java.awt.image.DataBufferInt;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import org.apache.commons.io.FileUtils;

import edu.semeru.android.gcat.helpers.HungarianAlgorithm;
import edu.semeru.android.gcat.helpers.ImagesHelper;
import edu.semeru.android.gcat.tree_builder.BasicTreeNode;
import edu.semeru.android.gcat.tree_builder.RootUINode;
import edu.semeru.android.gcat.tree_builder.UIDumpParser;
import edu.semeru.android.gcat.tree_builder.UiTreeNode;

/**
 *
 * @author jhoskins
 */
public class ScreenMatcher {

    ArrayList<Screen> screens;
    String dataRoot;

    public ScreenMatcher(String dataRoot) {
        this.screens = new ArrayList();
        this.dataRoot = dataRoot;
    }

    /**
     * Read and map all the xml files to png files by version and step number.
     */
    public void loadScreens() {
        HashMap<String, String> pngMap = new HashMap();

        File[] pngFiles = new File(dataRoot + File.separator + "screenshots").listFiles(new FilenameFilter() {
            @Override
            public boolean accept(File file, String fName) {
                return fName.toLowerCase().endsWith(".png");
            }
        });

        String filename;
        String version;
        int step;
        String key; // Concatenation of version and step for use as hash key.
        for (File pngFile : pngFiles) {
            filename = pngFile.getAbsolutePath();

            version = Screen.extractVersionNumber(filename);
            step = Screen.extractStepNumber(filename);

            key = version + "," + step;
            pngMap.put(key, filename);
        }

        File[] xmlFiles = new File(dataRoot).listFiles(new FilenameFilter() {
            @Override
            public boolean accept(File file, String fName) {
                return fName.toLowerCase().endsWith(".xml");
            }
        });

        for (File xmlFile : xmlFiles) {
            filename = xmlFile.getAbsolutePath();

            version = Screen.extractVersionNumber(filename);
            step = Screen.extractStepNumber(filename);

            key = version + "," + step;
            if (pngMap.containsKey(key)) {
                Screen newScreen = new Screen();
                newScreen.setImgPath(pngMap.get(key));
                newScreen.setXmlPath(filename);
                //System.out.println(newScreen.getActivity());
                screens.add(newScreen);
            }
        }
    }

    /**
     * Get the screens for each activity that have the lowest step numbers.
     *
     * @return
     */
    public ArrayList<Screen> getMinStepScreens() {
        if (this.screens == null) {
            this.loadScreens();
        }

        String key; //Concatenation of version and activity.
        HashMap<String, Screen> activityMap = new HashMap();
        for (Screen screen : this.screens) {
            if (screen.getWindow().contains("ALERT")) {
                continue;
            }

            key = screen.getWindow() + "," + screen.getVersion();
            //System.out.println("Activity: " + screen.getActivity());
            //System.out.println("window: " + screen.getWindow());

            if (activityMap.containsKey(key)) {
                if (screen.getStepNumber() < activityMap.get(key).getStepNumber()) {
                    activityMap.put(key, screen);
                }
            } else {
                activityMap.put(key, screen);
            }
        }

        return new ArrayList(activityMap.values());
    }

    /**
     * Sort a list of screens into a 2d array where arr[0] are old screens, and
     * arr[1] are new.
     *
     * @param screens
     * @return
     */
    public ArrayList<ArrayList<Screen>> sortByVersion(ArrayList<Screen> screens) {

        // Parse out the versions
        ArrayList<String> versions = new ArrayList();
        for (Screen screen : screens) {
            if (!versions.contains(screen.getVersion())) {
                versions.add(screen.getVersion());
            }
        }

        Collections.sort(versions);

        ArrayList<Screen> oldScreens = new ArrayList();
        ArrayList<Screen> newScreens = new ArrayList();

        for (Screen screen : screens) {
            if (screen.getVersion().equals(versions.get(0))) {
                oldScreens.add(screen);
            } else {
                newScreens.add(screen);
            }
        }
        ArrayList<ArrayList<Screen>> allScreens = new ArrayList();
        allScreens.add(oldScreens);
        allScreens.add(newScreens);
        return allScreens;
    }

    public ArrayList<Screen> getScreens() {
        return this.screens;
    }
    
    
//    public double getEdgeDifference(BufferedImage bf1, BufferedImage bf2) throws IOException, Exception {
//
//        ColorProcessor ip1 = new ColorProcessor(bf1);
//        ip1.findEdges();
//        ColorProcessor ip2 = new ColorProcessor(bf2);
//        ip2.findEdges();
//
//        BufferedImage screen1 = ip1.getBufferedImage();
//        BufferedImage screen2 = ip2.getBufferedImage();
//        
//        ImageIO.write(screen1, "png", new File("/home/jhoskins/matchtmp/bf1.png"));
//        ImageIO.write(screen2, "png", new File("/home/jhoskins/matchtmp/bf2.png"));
//        
//        SsimCalculator sc = new SsimCalculator(new File("/home/jhoskins/matchtmp/bf1.png"));
//        return sc.compareTo(new File("/home/jhoskins/matchtmp/bf2.png"));
//        
////        int[] pixels1 = ((DataBufferInt) screen1.getRaster().getDataBuffer()).getData(); // Flatten the image, taken from https://stackoverflow.com/questions/6524196/java-get-pixel-array-from-image
////        int[] pixels2 = ((DataBufferInt) screen2.getRaster().getDataBuffer()).getData(); // Flatten the image, taken from https://stackoverflow.com/questions/6524196/java-get-pixel-array-from-image
////
////        if (pixels2.length != pixels1.length) {
////            throw new Exception("Images must have the same dimensions.");
////        }
////
////        // Does a very basic pixel comparison. This could likely be improved.
////        int simpix = 0;
////        int totalpix = 0;
////        int BLACK = -16777216;
////        for (int i = 0; i < pixels1.length; i++) {
////            if (pixels1[i] != BLACK && pixels2[i] != BLACK) {
////                simpix++;
////            } else if (pixels1[i] != BLACK || pixels2[i] != BLACK) {
////                totalpix++;
////            }
////        }
////
////        double diff = simpix / (double) (totalpix + simpix);
////        return diff;
//    }
    
    public double getPixelDifference(BufferedImage bf1, BufferedImage bf2) throws Exception {

    
        byte[] pixels1 = ((DataBufferByte) bf1.getRaster().getDataBuffer()).getData(); // Flatten the image, taken from https://stackoverflow.com/questions/6524196/java-get-pixel-array-from-image
        byte[] pixels2 = ((DataBufferByte) bf2.getRaster().getDataBuffer()).getData(); // Flatten the image, taken from https://stackoverflow.com/questions/6524196/java-get-pixel-array-from-image

        if (pixels2.length != pixels1.length) {
            throw new Exception("Images must have the same dimensions.");
        }

        // Does a very basic pixel comparison. This could likely be improved.
        double diff = 0;
        for (int i = 0; i < pixels1.length; i++) {
            Color c1 = ImagesHelper.intToArgb(pixels1[i]);
            Color c2 = ImagesHelper.intToArgb(pixels2[i]);
            double colorDistance = ImagesHelper.colorDistance(c1, c2);
            diff += colorDistance;
        }


        double normalizedDiff = diff / (double) pixels1.length / 764.834; // max color distance
        return normalizedDiff;
    }

    /**
     * Get the pixel difference of two same dimensional images.
     *
     * @param image1
     * @param image2
     * @param topCrop the x distance of screen to cut off the top, used to get
     * rid of the notification bar.
     * @return Amount of different pixels.
     * @throws IOException
     * @throws Exception
     */
    public double getScreenDifference(Screen s1, Screen s2, int topCrop) throws IOException, Exception {
        
        File image1 = new File(s1.getImgPath());
        File image2 = new File(s2.getImgPath());
        
        BufferedImage screen1 = ImageIO.read(image1);
        BufferedImage screen2 = ImageIO.read(image2);

        // Crop and flatten the images
        screen1 = ImagesHelper.cropImage(image1.getAbsolutePath(), 0, topCrop, screen1.getWidth(), screen1.getHeight());
        screen2 = ImagesHelper.cropImage(image2.getAbsolutePath(), 0, topCrop, screen2.getWidth(), screen2.getHeight());

        double pixelDiff = getPixelDifference(screen1, screen2);
        //double edgeDiff = getEdgeDifference(screen1, screen2);
        //double edgeDiff = 0;
        
        BufferedImage bi1 = makeBboxCoverage(new File(s1.getXmlPath()));
        BufferedImage bi2 = makeBboxCoverage(new File(s2.getXmlPath()));
        double bboxDiff = getPixelDifference(bi1, bi2);
                
        System.out.println(pixelDiff + " " + bboxDiff);
        return pixelDiff + bboxDiff;
    }
    
    public BufferedImage makeBboxCoverage(File xml1) {
        
        BufferedImage bf = new BufferedImage(1200, 1870, BufferedImage.TYPE_3BYTE_BGR);
        Graphics2D graph = bf.createGraphics();
        graph.setColor(Color.WHITE);
        
        
        UIDumpParser udp = new UIDumpParser();
        udp.parseXml(xml1.getAbsolutePath());
        ArrayList<UiTreeNode> leafNodes = udp.getLeafNodes();
        for (UiTreeNode node : leafNodes) {
            if (node.getWidth() * node.getHeight() < 100000) // Arbitrary
                graph.fill(new Rectangle(node.getX(), node.getY(), node.getWidth(), node.getHeight()));
        }
        graph.dispose();
        return bf;
    }

    public static void runMatcher(String dataRoot, String targetRoot) {

        // Within the repo the data is at /Artifacts/testDataMapped1-1xmlpng
        // Read and map all the xml files to png files by version and step number.
        ScreenMatcher sm = new ScreenMatcher(dataRoot);
        sm.loadScreens();
        System.out.println("Loaded!");

        // Get the screens for each activity that have the lowest step numbers, and 
        // sort them into a 2d array where arr[0] are old screens, and arr[1] are new.
        ArrayList<ArrayList<Screen>> allScreens = sm.sortByVersion(sm.getMinStepScreens());

        int dim = Math.max(allScreens.get(0).size(), allScreens.get(1).size());
        double[][] costMatrix = new double[dim][dim];

        System.out.println("BUilding cost...");
        for (int i = 0; i < allScreens.get(0).size(); i++) {
            Screen screen1 = allScreens.get(0).get(i);
            for (int j = 0; j < allScreens.get(1).size(); j++) {
                Screen screen2 = allScreens.get(1).get(j);
                try {
                    costMatrix[i][j] = sm.getScreenDifference(screen1, screen2, 50);
                } catch (Exception ex) {
                    Logger.getLogger(ScreenMatcher.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

        if (allScreens.get(0).isEmpty() || allScreens.get(1).isEmpty()) {
            System.out.println("No Matches found for: " + dataRoot);
            return;
        }

        HungarianAlgorithm ha = new HungarianAlgorithm(costMatrix);
        int[] result = ha.execute();

        Screen s1 = allScreens.get(0).get(0);
        Screen s2 = allScreens.get(1).get(0);

        String[] split = dataRoot.split(File.separator);
        String newFolder = targetRoot + File.separator + split[split.length - 1];
        new File(newFolder).mkdirs();
        System.out.println(newFolder);

        for (int i = 0; i < allScreens.get(0).size() && i < allScreens.get(1).size(); i++) {
            if (result[i] != -1 && i < result.length && i < allScreens.get(0).size() && result[i] < allScreens.get(1).size()) {// -1 denotes this screen has no closest match. Would appear if your previous set of images was larger than your new set.
                Screen oldScreen = allScreens.get(0).get(i);
                Screen newScreen = allScreens.get(1).get(result[i]);

                try {
                    FileUtils.copyFile(new File(oldScreen.getXmlPath()), new File(newFolder + File.separator + "uiautomator-" + oldScreen.getVersion() + "-" + i + ".xml"));
                    FileUtils.copyFile(new File(oldScreen.getImgPath()), new File(newFolder + File.separator + "screen-" + oldScreen.getVersion() + "-" + i + ".png"));
                    FileUtils.copyFile(new File(newScreen.getXmlPath()), new File(newFolder + File.separator + "uiautomator-" + newScreen.getVersion() + ".xml"));
                    FileUtils.copyFile(new File(newScreen.getImgPath()), new File(newFolder + File.separator + "screen-" + newScreen.getVersion() + "-" + i + ".png"));
                } catch (IOException ex) {
                    Logger.getLogger(ScreenMatcher.class.getName()).log(Level.SEVERE, null, ex);
                }

            }
        }

        System.out.println("Done!");
    }

    public static void main(String args[]) throws IOException {
        String folderRoot = "/home/jhoskins/newFolders"; // MUST BE A FOLDER TANSFORMED BY THE CSOutputTransform!
        String targetRoot = "/home/jhoskins/outStuff2";
        
        File file = new File(folderRoot);
        String[] directories = file.list(new FilenameFilter() {
            @Override
            public boolean accept(File current, String name) {
                return new File(current, name).isDirectory();
            }
        });
        
        for (String s : directories) {
            String place = folderRoot + File.separator + s;
            
            runMatcher(place, targetRoot);
        }


        
        



//        String path1 = "/home/jhoskins/newFolders/net.fred.feedex-1.9.0-vs-1.9.1-Bottom_Up/screenshots/net.fred.feedex.Bottom_Up.Expected..1.net.fred.feedex_1.9.1-22.png";
////        String path2 = "/home/jhoskins/newFolders/net.fred.feedex-1.9.0-vs-1.9.1-Bottom_Up/screenshots/net.fred.feedex.Bottom_Up.Expected..1.net.fred.feedex_1.9.1-26.png";
//        String xml1 = "/home/jhoskins/newFolders/com.a5corp.weather-4.1-vs-5.3.2-Bottom_Up/com.a5corp.weather-4.1-1--com.a5corp.weather.activity.WeatherActivity--FRAGMENT:WeatherFragment-Expected-Bottom_Up-5.xml";
//        String xml2 = "/home/jhoskins/newFolders/com.a5corp.weather-4.1-vs-5.3.2-Bottom_Up/com.a5corp.weather-5.3.2-1--com.a5corp.weather.activity.WeatherActivity--FRAGMENT:WeatherFragment-Expected-Bottom_Up-5.xml";
//        String img1 = "/home/jhoskins/newFolders/com.a5corp.weather-4.1-vs-5.3.2-Bottom_Up/screenshots/com.a5corp.weather.Bottom_Up.Expected..1.com.a5corp.weather_4.1-5.png";
//        String img2 = "/home/jhoskins/newFolders/com.a5corp.weather-4.1-vs-5.3.2-Bottom_Up/screenshots/com.a5corp.weather.Bottom_Up.Expected..1.com.a5corp.weather_5.3.2-5.png";
//                
////        String path1 = "/home/jhoskins/out1.png";
//        String path2 = "/home/jhoskins/out2.png";

//
//        Screen s1 = new Screen();
//        s1.setXmlPath(xml1);
//        s1.setImgPath(img1);
//        
//        Screen s2 = new Screen();
//        s2.setXmlPath(xml2);
//        s2.setImgPath(img2);
//
//        ScreenMatcher sm = new ScreenMatcher("");
//        try {
//            sm.getScreenDifference(s1, s2, 50);
//            
//            
//            //BufferedImage bf = ImageIO.read(new File(path2));
////        //bf = ImagesHelper.cropImage(path2, 0, 50, bf.getWidth(), bf.getHeight());
//        //ImageIO.write(bf, "png", new File("/home/jhoskins/out2.png"));
////        
//        ScreenMatcher sm = new ScreenMatcher("");
//        sm.makeBboxCoverage(new File(xml1), "oout1.png");
//        sm.makeBboxCoverage(new File(xml2), "oout2.png");
//        
////        SsimCalculator sc = new SsimCalculator(new File(path1));
////        System.out.println(sc.compareTo(new File(path2)));
////        
//        try {
//            sm.getImageDifference(new File("/home/jhoskins/oout1.png"), new File("/home/jhoskins/oout2.png"), 50);
//        } catch (Exception ex) {
//            Logger.getLogger(ScreenMatcher.class.getName()).log(Level.SEVERE, null, ex);
//        }

//// System.out.println(ImagesHelper.colorDistance(new Color(0, 0, 0), new Color(255, 255, 255)));
//
//// TODO try edge detection + SSIM http://www.ee.cityu.edu.hk/~lmpo/publications/2006_ICASSP_EBSSIM.pdf
//// Try hausdorff distance.
//        } catch (Exception ex) {
//            Logger.getLogger(ScreenMatcher.class.getName()).log(Level.SEVERE, null, ex);
//        }
    }
}
