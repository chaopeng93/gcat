/*******************************************************************************
 * Copyright (c) 2016, SEMERU
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *******************************************************************************/

package edu.semeru.android.gcat.screen_matching;

import java.io.File;
import java.io.FilenameFilter;

/**
 *
 * @author jhoskins
 */
public class Screen {
    
    private String xmlPath;
    private String imgPath;
    private String version;
    private String activity;
    private String window;
    private String androidPackage;
    private int stepNumber;
    
    public Screen() {
        this.xmlPath = null;
        this.imgPath = null;
        this.activity = null;
        this.window = null;
        this.version = null;
        this.androidPackage = null;
        this.stepNumber = -1;
    }
    
    public static String extractActivityName(String filepath) {
        assert filepath.endsWith(".xml");
        String[] split = filepath.split(File.separator);
        String filename = split[split.length - 1];
        return filename.split("-")[4];
    }
    
    public static String extractWindowName(String filepath) {
        assert filepath.endsWith(".xml");
        String[] split = filepath.split(File.separator);
        String filename = split[split.length - 1];
        return filename.split("-")[6];
    }
    
    public static int extractStepNumber(String filename) {
        String[] split;
        if (filename.endsWith(".xml"))
            split = filename.split("-");
        else
            split = filename.split("-");
        
        String stepNumber = split[split.length - 1].split("\\.")[0];
        return Integer.parseInt(stepNumber);
        
    }
    
    public static String extractVersionNumber(String filename) {
        String[] split = filename.split(File.separator);
        
        String splitString;
        if (filename.endsWith(".xml")) {
            return split[split.length - 1].split("-")[1];
        } else {
            String[] split2 = split[split.length - 1].split("_");
            return split2[split2.length - 1].split("\\-")[0];
        }
    }
    
    public static String extractPackageName(String filepath) {
        assert filepath.endsWith(".xml");
        String[] split = filepath.split(File.separator);
        String filename = split[split.length - 1];
        
        return filename.split("--")[0].split("-")[0];
    }
    
    public String getXmlPath() {
        return this.xmlPath;
    }
    
    public void setXmlPath(String xmlPath) {
        this.xmlPath = xmlPath;
        this.stepNumber = extractStepNumber(xmlPath);
        this.version = extractVersionNumber(xmlPath);
        this.activity = extractActivityName(xmlPath);
        this.window = extractWindowName(xmlPath);
        this.androidPackage = extractPackageName(xmlPath);
    }
    
    public String getImgPath() {
        return this.imgPath;
    }
    
    public void setImgPath(String imgPath) {
        this.imgPath = imgPath;
        this.stepNumber = extractStepNumber(imgPath);
        this.version = extractVersionNumber(imgPath);
    }
    
    public int getStepNumber() {
        return this.stepNumber;
    }
    
    public String getVersion() {
        return this.version;
    }
    
    public String getActivity() {
        return this.activity;
    } 
    
    public String getPackageName() {
        return this.androidPackage;
    }
    
    public String getWindow() {
        return this.window;
    }
}
