/*******************************************************************************
 * Copyright (c) 2016, SEMERU
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *******************************************************************************/

package edu.semeru.android.gcat.changes;

/**
 * All possible types of violations.
 * @author Boyang Li Created on Nov 07, 2016
 */
public enum ChangeType {

    /**
     *
     */
    LOCATION_CHANGE {
        @Override
        public String toString() {
            return "Location Change";
        }    
    },

    
    /**
     *
     */
    MISSING_COMPONENT {
        @Override
        public String toString() {
            return "Missing Component Violation";

        }
    },


    /**
     *
     */
    COMPONENT_SIZE {
        @Override
        public String toString() {
            return "Size Violation";
        }
    },

    /**
     *
     */
    COLOR_MISMATCH {
        @Override
        public String toString() {
            return "Color Mismatch Violation";
        }
    },

    /**
     *
     */
    Text_MISMATCH {
        @Override
        public String toString() {
            return "Text Mismatch Violation";
        }
    },

    /**
     *
     */
    Extra_Component {
        @Override
        public String toString() {
            return "Extra Component Violation";
        }
    },

    /**
     *
     */
    Font_MISMATCH {
        @Override
        public String toString() {
            return "Font violation";
        }
    },

    /**
     *
     */
    TEXT_COLOR {
        @Override
        public String toString() {
            return "Text color violation";
        }
    },
    Image_DIFF {
        @Override
        public String toString() {
            return "Image violation";
        }
    },
}
