/*******************************************************************************
 * Copyright (c) 2016, SEMERU
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *******************************************************************************/

package edu.semeru.android.gcat.changes;

import edu.semeru.android.gcat.tree_builder.UiTreeNode;

/**
 * Violation that describes the case of when a component is deleted from the GUI.
 * @author Boyang Li Created on Nov 07, 2016
 */
public class MissingComponentChange extends Change {

    /**
     * The missing node in design
     */
    UiTreeNode missingNode = null;

    /**
     *
     * @param missingNode node from old commit that has been deleted
     */
    public MissingComponentChange(UiTreeNode missingNode) {
        super(ChangeType.MISSING_COMPONENT);
        this.missingNode = missingNode;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("The "+ missingNode.getType() + " at location " + "(" + missingNode.getX() + "," + missingNode.getY() + ") was removed!");
        return sb.toString();
    }
     
    /**
     * Get the old GUI version's newNode that's associated with the violation. 
     *
     * @return missing node from old commit
     */
    @Override
    public UiTreeNode getOldNode() {
        return missingNode;
    }
    
    /**
     * Get the new GUI version's newNode that's associated with the violation. 
     *
     * @return null, no corresponding node in new commit
     */
    @Override
    public UiTreeNode getNewNode() {
        return null;
    }

    /**
     *
     * @return true
     */
    @Override
    public boolean isExceededThreshold() {
        return true;
    }
}
