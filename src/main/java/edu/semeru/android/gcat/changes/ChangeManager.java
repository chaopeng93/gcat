/*******************************************************************************
 * Copyright (c) 2016, SEMERU
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *******************************************************************************/

package edu.semeru.android.gcat.changes;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import edu.semeru.android.gcat.helpers.ImagesHelper;
import edu.semeru.android.gcat.matching.MatchingAnalysis;
import edu.semeru.android.gcat.nlgeneration.ReportContainer;
import edu.semeru.android.gcat.nlgeneration.SummaryGenerator;
import edu.semeru.android.gcat.tree_builder.UiTreeNode;

/**
 * This class manages all violations, keeping track of them, and providing access
 * to subsets of them.
 *
 * @author Boyang Li Created on Nov 06, 2016
 */
public class ChangeManager {
    
    private HashMap<UiTreeNode, ArrayList<Change>> violationMap = new HashMap<UiTreeNode, ArrayList<Change>>();
    private HashSet<MissingComponentChange> missingVlSet = new HashSet<MissingComponentChange>();
    private String outputFolder;
    private final String combinedScreenshotPath = "res" + 
            File.separator + "images" + File.separator + "Changes-augmented.png";
    
    /**
     * The parameters are used for generate a violation report
     *
     * @param androidScreenshotLocation
     * @param outputFolder string of outputFolder file path
     */
    public ChangeManager(String androidScreenshotLocation, String outputFolder) {
        this.outputFolder = outputFolder;
    }

    /**
     * Add and store violation vl
     *
     * @param vl violation
     */
    public void addViolations(Change vl) {
        if (vl instanceof MissingComponentChange) {
            missingVlSet.add((MissingComponentChange) vl);
        } else {
            UiTreeNode uiNode = vl.getNewNode();
            if (violationMap.containsKey(uiNode)) {
                violationMap.get(uiNode).add(vl);
                Collections.sort(violationMap.get(uiNode));
            } else {
                ArrayList<Change> newList = new ArrayList<Change>();
                newList.add(vl);
                violationMap.put(uiNode, newList);
            }
        }
    }

    /**
     * Gets the number of violations associated with a given node.
     * @param uiNode node to check for associated violations
     * @return number of violations
     */
    public int getNumberOfValidViolation(UiTreeNode uiNode) {
        ArrayList<Change> violations = this.violationMap.get(uiNode);
        if (violations == null) {
            return 0;
        } else {
            int counter = 0;
            for (Change v : violations) {
                if (v.isExceededThreshold()) {
                    counter++;
                }
            }
            return counter;
        }

    }
    /**
     * 
     * Parses through list of all violations and creates the ReportContainer 
     * (that holds the natural language log and 2 cropped images)
     * 
     * Prints all the changes after adding them.
     * 
     * @param previousImg path of old commit screenshot
     * @param newImg path of new commit screenshot
     * @param name
     */
    
    public void printOutViolations(String previousImg, String newImg, String name) {
        ReportContainer report = new ReportContainer();
        List<Change> allviolations = this.getAllViolations();
        allviolations.addAll(this.getMissingComponents());
        SummaryGenerator summary = new SummaryGenerator(allviolations);
        report.setOldScreenshotPath(previousImg);
        report.setNewScreenshotPath(newImg);
        report.setCombinedScreenshotPath(combinedScreenshotPath);
        
        for(Change current: allviolations) {
            report.addChange(current);
        } 
        
        report.setSummary(summary.generateSummary(report.getViolationCounter()));
        try{
            report.printAllChanges();
            ImagesHelper.augmentScreenShot(MatchingAnalysis.newImageJpg, "html" + File.separator + combinedScreenshotPath, allviolations);
        }catch(IOException e){
            e.printStackTrace();
        }
    }


    /**
     * Check to see if the violation manager has a certain violation type
     *
     * @param type type of violation to check for
     * @return boolean
     */
    public boolean hasViolationType(ChangeType type) {
        for (Map.Entry<UiTreeNode, ArrayList<Change>> entry : violationMap.entrySet()) {
            ArrayList<Change> allViolations = entry.getValue();
            for (Change v : allViolations) {
                if (v.type == type) {
                    return true;
                }
            }
        }
        if (missingVlSet.size() > 0) {
            return true;
        }
        return false;
    }

    /**
     * Get a List of all violations of a specific type
     *
     * @param type type of violations
     * @return list of violations of specified type
     */
    public List<Change> getViolationsOfType(ChangeType type) {
        List<Change> violationsList = new ArrayList<Change>();
        for (Map.Entry<UiTreeNode, ArrayList<Change>> entry : violationMap.entrySet()) {
            ArrayList<Change> allViolations = entry.getValue();
            for (Change v : allViolations) {
                if (v.type == type) {
                    violationsList.add(v);
                }
            }
        }
        return violationsList;
    }
    
    /**
     * Get a List of all violations
     * 
     * @return list of all violations
     */
    public List<Change> getAllViolations() {
        List<Change> violationsList = new ArrayList<Change>();
        for (Map.Entry<UiTreeNode, ArrayList<Change>> entry : violationMap.entrySet()) {
            ArrayList<Change> allViolations = entry.getValue();
            for (Change v : allViolations) {
                violationsList.add(v);
            }
        }
        return violationsList;
    }

    /**
     * Get a List of all missing components
     *
     * @return list of all missing components
     */
    public List<MissingComponentChange> getMissingComponents() {
        List<MissingComponentChange> missingList = new ArrayList<MissingComponentChange>();

        for (MissingComponentChange v : missingVlSet) {
            missingList.add(v);
        }
        return missingList;
    }

    /*    private String getColorBlock(String color) {
     * String returnStr = "<div class=\"input-color\"> <input type=\"text\" value=\"" + color + "\" />";
     * returnStr += "<div class=\"color-box\" style=\"background-color:" + color + ";\"></div></div>";
     * return returnStr;
     * }*/
}
